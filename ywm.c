#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <X11/cursorfont.h>
#include <X11/keysym.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#define DIE(...) { fprintf(stderr, __VA_ARGS__); exit(EXIT_FAILURE); }

typedef struct {
    Window window; // assumes valid window ids are always nonzero
                   // uses managed_window.window as truthy or falsy
    unsigned key;
    int pid; // TODO use at some point
    int is_active;
    float width_factor;
    float height_factor;
} managed_window;

typedef void (*tiling_policy)(managed_window *window, int index, int nwindows, unsigned width, unsigned height);

typedef union {
    char *const *argv;
    tiling_policy tiling_mode;
    size_t index;
    unsigned key;
    int value;
    struct {
        int x;
        int y;
    };
    struct {
        float xf;
        float yf;
    };
} arguments;

typedef struct {
    KeySym keysym;
    unsigned modifiers;
    void (*action)(managed_window *, arguments);
    arguments arguments;
} key_binding;

const unsigned BORDER_WIDTH = 1;
const unsigned BORDER_COLOR_FOCUSED = 0x00ff00;
const unsigned BORDER_COLOR_UNFOCUSED = 0xff0000;
#define MAX_WINDOWS 200

Display *display;
Window root_window;
int running;

managed_window managed_windows[MAX_WINDOWS];

tiling_policy tiling_mode;
void draw_windows();

int max(int x, int y) {
    return x >= y ? x : y;
}

// Moves and resizes window, i.e. puts it in a rectangle
// If the window has smaller max_width or max_height than the rectangle, the window will be centered within the rectangle
void move_resize(Window window, int x, int y, unsigned width, unsigned height) {
    XSizeHints hints;
    XGetNormalHints(display, window, &hints);

    width -= 2*BORDER_WIDTH;
    height -= 2*BORDER_WIDTH;

    if(hints.max_width && hints.max_width < width) {
        x += (width - hints.max_width) / 2;
        width = hints.max_width;
    }
    if(hints.max_height && hints.max_height < height) {
        y += (height - hints.max_height) / 2;
        height = hints.max_height;
    }

    XMoveResizeWindow(display, window, x, y, width, height);
}

void set_border(Window window, unsigned border_width, unsigned border_color) {
    if(window != PointerRoot && window != None) {
        XSetWindowBorderWidth(display, window, border_width);
        XSetWindowBorder(display, window, border_color);
    }
}

Window focused_window() {
    Window window;
    int _;
    XGetInputFocus(display, &window, &_);
    return window;
}

void focus(Window window) {
    XSetInputFocus(display, window, RevertToParent, CurrentTime);
    draw_windows();
}

// Ensures that the given window is mapped
// Returns 1 if the window was newly mapped, 0 if the window was mapped already
int show_window(managed_window *window) {
    if(!window->is_active) {
        window->is_active = 1;
        XMapWindow(display, window->window);
        focus(window->window);
        return 1;
    }
    else {
        return 0;
    }
}

// Ensures that the given window is unmapped
// Return 1 if the window was newly unmapped, 0 if the window was unmapped already
int hide_window(managed_window *window) {
    if(window->is_active) {
        window->is_active = 0;
        XUnmapWindow(display, window->window);
        return 1;
    }
    else {
        return 0;
    }
}

void quit(managed_window *_, arguments __) {
    running = 0;
}

// Sends a close request to the current window
// This is less violent than XDestroyWindow, because the window may respond with a close dialog
void close_window(managed_window *window, arguments __) {
    XEvent message = {.xclient = {
        .type = ClientMessage,
        .window = window->window,
        .message_type = XInternAtom(display, "WM_PROTOCOLS", True),
        .format = 32,
        .data.l = {XInternAtom(display, "WM_DELETE_WINDOW", True), CurrentTime},
    }};
    XSendEvent(display, window->window, False, NoEventMask, &message);
}

void spawn(managed_window *_, arguments arguments) {
    if(fork() == 0) {
        execvp(arguments.argv[0], arguments.argv);
        DIE("execvp failed\n");
    }
}

// Moves focus to window, or hides it if it is already focused
void toggle_window_by_index(managed_window *_, arguments arguments) {
    managed_window *window = &managed_windows[arguments.index];
    if(!window->window) {
        return;
    }

    if(focused_window() == window->window) {
        hide_window(window);
        draw_windows();
    }
    else if(window->is_active) {
        focus(window->window);
    }
    else {
        show_window(window);
    }
}

void show_windows_by_key(managed_window *_, arguments arguments) {
    int some_new_window_shown = 0;
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window && managed_windows[i].key == arguments.key && show_window(&managed_windows[i])) {
            some_new_window_shown = 1;
        }
    }

    if(some_new_window_shown) {
        draw_windows();
    }
}

void hide_windows_by_key(managed_window *_, arguments arguments) {
    int some_new_window_hidden = 0;
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window && managed_windows[i].key == arguments.key && hide_window(&managed_windows[i])) {
            some_new_window_hidden = 1;
        }
    }

    if(some_new_window_hidden) {
        draw_windows();
    }
}

// Shows all windows with assigned key.
// Only if all windows with assigned key are already shown, hides hides all windows with assigned key instead.
void toggle_windows_by_key(managed_window *_, arguments arguments) {
    int some_window_not_already_shown = 0;
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window && managed_windows[i].key == arguments.key && !managed_windows[i].is_active) {
            some_window_not_already_shown = 1;
            break;
        }
    }

    (some_window_not_already_shown ? show_windows_by_key : hide_windows_by_key)(_, arguments);
}

void claim_window_by_key(managed_window *current_window, arguments arguments) {
    if(current_window) {
        current_window->key = arguments.key;
    }
}

void hide_all_windows(managed_window *_, arguments __) {
    int some_new_window_hidden = 0;
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window && hide_window(&managed_windows[i])) {
            some_new_window_hidden = 1;
        }
    }

    if(some_new_window_hidden) {
        draw_windows();
    }
}

void set_tiling_mode(managed_window *_, arguments arguments) {
    tiling_mode = arguments.tiling_mode;
    draw_windows();
}

const unsigned INITIAL_GAP_WIDTH = 30;
const unsigned INITIAL_GAP_HEIGHT = 30;

unsigned GAP_WIDTH = INITIAL_GAP_WIDTH;
unsigned GAP_HEIGHT = INITIAL_GAP_WIDTH;

void set_gaps(unsigned width, unsigned height) {
    GAP_WIDTH = width;
    GAP_HEIGHT = height;
    draw_windows();
}

void change_gaps_by(managed_window *_, arguments arguments) {
    set_gaps(GAP_WIDTH + arguments.x, GAP_HEIGHT + arguments.y);
}

void toggle_gaps(managed_window *_, arguments arguments) {
    static unsigned last_gap_width = INITIAL_GAP_WIDTH;
    static unsigned last_gap_height = INITIAL_GAP_HEIGHT;

    if(GAP_WIDTH == 0 && GAP_HEIGHT == 0) { // if gaps are turned off, use most recent gapsize
        set_gaps(last_gap_width, last_gap_height);
    }
    else { // otherwise save gapsize and turn off gaps
        last_gap_width = GAP_WIDTH;
        last_gap_height = GAP_HEIGHT;
        set_gaps(0, 0);
    }
}

void change_size_factors_by(managed_window *window, arguments arguments) {
    if(window) {
        window->width_factor += arguments.xf;
        window->height_factor += arguments.yf;
        draw_windows();
    }
}

void horizontal(managed_window *window, int index, int nwindows, unsigned width, unsigned height) {
    unsigned usable_width = max(width - (nwindows + 1) * GAP_WIDTH, 0);
    unsigned usable_height = max(height - 2 * GAP_HEIGHT, 0);

    move_resize(window->window, index * (usable_width/nwindows) + (index+1) * GAP_WIDTH, GAP_HEIGHT, usable_width/nwindows, usable_height);
}

void vertical(managed_window *window, int index, int nwindows, unsigned width, unsigned height) {
    unsigned usable_width = max(width - 2 * GAP_WIDTH, 0);
    unsigned usable_height = max(height - (nwindows + 1) * GAP_HEIGHT, 0);

    move_resize(window->window, GAP_WIDTH, index * (usable_height/nwindows) + (index+1) * GAP_HEIGHT, usable_width, usable_height/nwindows);
}

// Default tiling mode of most tiling window managers
// The left half of the screen is reserved for the master (first) window
// The right half is used for the vertically tiled slave stack (remaining windows)
void master_with_vertical_stack(managed_window *window, int index, int nwindows, unsigned width, unsigned height) {
    if(nwindows == 0) {
        return;
    }
    else if(nwindows == 1) {
        move_resize(window->window, GAP_WIDTH, GAP_HEIGHT, width - 2 * GAP_WIDTH, height - 2 * GAP_HEIGHT);
    }
    else {
        unsigned column_width = max((width - 3 * GAP_WIDTH) / 2, 0); // Width for master and slave columns
        if(index == 0) {
            unsigned master_height = max(height - 2 * GAP_HEIGHT, 0);
            move_resize(window->window, GAP_WIDTH, GAP_HEIGHT, column_width, master_height);
        }
        else {
            int slave_index = index - 1;
            int nslaves = nwindows - 1;
            unsigned single_slave_height = max((height - (nslaves + 1) * GAP_HEIGHT) / nslaves, 0);
            move_resize(window->window, column_width + 2 * GAP_WIDTH, slave_index * (single_slave_height + GAP_HEIGHT) + GAP_HEIGHT, column_width, single_slave_height);
        }
    }
}

char *const SuperReturn_args[] = {"st", NULL};

const unsigned ModMask = Mod1Mask;
key_binding key_bindings[] = {
    {.keysym = XK_q, .modifiers = ModMask | ShiftMask, .action = quit},
    {.keysym = XK_c, .modifiers = ModMask | ShiftMask, .action = close_window},
    {.keysym = XK_Escape, .modifiers = ModMask, .action = hide_all_windows},

    {.keysym = XK_Return, .modifiers = ModMask, .action = spawn, .arguments = {.argv = SuperReturn_args}},
    {.keysym = XK_v, .modifiers = ModMask, .action = set_tiling_mode, .arguments = {.tiling_mode = vertical}},
    {.keysym = XK_h, .modifiers = ModMask, .action = set_tiling_mode, .arguments = {.tiling_mode = horizontal}},

    {.keysym = XK_minus, .modifiers = ModMask | ControlMask, .action = change_gaps_by, .arguments = {.x = -5, .y = -5}},
    {.keysym = XK_plus, .modifiers = ModMask | ControlMask, .action = change_gaps_by, .arguments = {.x = +5, .y = +5}},
    {.keysym = XK_Left, .modifiers = ModMask | ControlMask, .action = change_gaps_by, .arguments = {.x = -5}},
    {.keysym = XK_Right, .modifiers = ModMask | ControlMask, .action = change_gaps_by, .arguments = {.x = +5}},
    {.keysym = XK_Down, .modifiers = ModMask | ControlMask, .action = change_gaps_by, .arguments = {.y = -5}},
    {.keysym = XK_Up, .modifiers = ModMask | ControlMask, .action = change_gaps_by, .arguments = {.y = +5}},
    {.keysym = XK_0, .modifiers = ModMask | ControlMask, .action = toggle_gaps},

    {.keysym = XK_Left, .modifiers = ModMask | ShiftMask, .action = change_size_factors_by, .arguments = {.xf = -0.05}},
    {.keysym = XK_Right, .modifiers = ModMask | ShiftMask, .action = change_size_factors_by, .arguments = {.xf = +0.05}},
    {.keysym = XK_Down, .modifiers = ModMask | ShiftMask, .action = change_size_factors_by, .arguments = {.yf = -0.05}},
    {.keysym = XK_Up, .modifiers = ModMask | ShiftMask, .action = change_size_factors_by, .arguments = {.yf = +0.05}},

    {.keysym = XK_1, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 0}},
    {.keysym = XK_2, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 1}},
    {.keysym = XK_3, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 2}},
    {.keysym = XK_4, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 3}},
    {.keysym = XK_5, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 4}},
    {.keysym = XK_6, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 5}},
    {.keysym = XK_7, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 6}},
    {.keysym = XK_8, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 7}},
    {.keysym = XK_9, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 8}},
    {.keysym = XK_0, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 9}},

    {.keysym = XK_a, .modifiers = ModMask | ShiftMask, .action = claim_window_by_key, .arguments = {.key = XK_a}},
    {.keysym = XK_a, .modifiers = ModMask, .action = toggle_windows_by_key, .arguments = {.key = XK_a}},
};

void grab_keys(Window);

managed_window *managed_window_by_id(Window window) {
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window == window) {
            return &managed_windows[i];
        }
    }
    return NULL;
}

// Creates a managed_window entry for a given window id
// Returns the address of the created entry or NULL if there is no space left
managed_window *add_window(Window window) {
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window == window) {
            return &managed_windows[i];
        }
        if(!managed_windows[i].window) {
            managed_windows[i] = (managed_window){.window = window, .key = 0, .pid = 0, .is_active = 0, .width_factor = 1.0, .height_factor = 1.0};
            return &managed_windows[i];
        }
    }
    return NULL;
}

void remove_window(Window window) {
    managed_window *managed = managed_window_by_id(window);
    if(managed) {
        *managed = (managed_window){.window = 0, .key = 0, .pid = 0, .is_active = 0};
    }
}

void draw_windows() {
    int width = XDisplayWidth(display, XDefaultScreen(display));
    int height = XDisplayHeight(display, XDefaultScreen(display));
    Window focused = focused_window();

    int nwindows = 0;
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window && managed_windows[i].is_active) {
            nwindows++;
        }
    }
    int index = 0;
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window && managed_windows[i].is_active) {
            tiling_mode(&managed_windows[i], index, nwindows, width, height);
            set_border(managed_windows[i].window, BORDER_WIDTH, managed_windows[i].window == focused ? BORDER_COLOR_FOCUSED : BORDER_COLOR_UNFOCUSED);
            index++;
        }
    }
}

tiling_policy tiling_mode = master_with_vertical_stack;

void on_create_notify(const XEvent *f) {
    const XCreateWindowEvent *e = &f->xcreatewindow;
    Window window = e->window;
    printf("CreateNotify %lu\n", window);
}

void on_destroy_notify(const XEvent *f) {
    const XDestroyWindowEvent *e = &f->xdestroywindow;
    Window window = e->window;
    printf("DestroyNotify %lu\n", window);

    remove_window(window);
    draw_windows();
}

void on_map_request(const XEvent *f) {
    const XMapRequestEvent *e = &f->xmaprequest;
    Window window = e->window;
    printf("MapRequest %lu\n", window);

    managed_window *managed_window = add_window(window); // TODO NULL handling
    show_window(managed_window);
    XSelectInput(display, window, EnterWindowMask | LeaveWindowMask);

    grab_keys(window);

    draw_windows();
}

void on_configure_request(const XEvent *f) {
    const XConfigureRequestEvent *e = &f->xconfigurerequest;

    XWindowChanges changes;
    changes.x = e->x;
    changes.y = e->y;
    changes.height = e->height;
    changes.width = e->width;
    changes.border_width = e->border_width;
    changes.sibling = e->above;
    changes.stack_mode = e->detail;

    XConfigureWindow(display, e->window, e->value_mask, &changes);
}

void on_enter_notify(const XEvent *f) {
    const XEnterWindowEvent *e = &f->xcrossing;
    Window window = e->window;
    printf("EnterNotify %lu\n", window);

    if(window != root_window) {
        focus(window);
    }
}

void on_leave_notify(const XEvent *f) {
    const XLeaveWindowEvent *e = &f->xcrossing;
    Window window = e->window;
    printf("LeaveNotify %lu\n", window);
}

void on_key_press(const XEvent *f) {
    const XKeyPressedEvent *e = &f->xkey;
    printf("KeyPress %u %u\n", e->keycode, e->state);

    for(int i = 0; i < sizeof(key_bindings) / sizeof(*key_bindings); i++) {
        if(XKeysymToKeycode(display, key_bindings[i].keysym) == e->keycode && key_bindings[i].modifiers == e->state) {
            managed_window *managed_window = managed_window_by_id(e->window);
            key_bindings[i].action(managed_window, key_bindings[i].arguments);
            break;
        }
    }
}

void (*event_handlers[LASTEvent])(const XEvent *) = {
    [CreateNotify] = on_create_notify,
    [DestroyNotify] = on_destroy_notify,
    [MapRequest] = on_map_request,
    [ConfigureRequest] = on_configure_request,
    [EnterNotify] = on_enter_notify,
    [LeaveNotify] = on_leave_notify,
    [KeyPress] = on_key_press
};

void grab_key(Window window, unsigned keycode, unsigned modifiers) {
    XGrabKey(display, keycode, modifiers, window, True, GrabModeAsync, GrabModeAsync);
}

void grab_keys(Window window) {
    for(int i = 0; i < sizeof(key_bindings) / sizeof(*key_bindings); i++) {
        grab_key(window, XKeysymToKeycode(display, key_bindings[i].keysym), key_bindings[i].modifiers);
    }
}

void handle_event(const XEvent *e) {
    if(event_handlers[e->type]) {
        event_handlers[e->type](e);
    }
}

int main() {
    display = XOpenDisplay(NULL); // TODO NULL
    if(display == NULL) {
        DIE("Could not open display");
    }
    root_window = DefaultRootWindow(display);

    // TODO check if other WM is already running
    XSelectInput(display, root_window, KeyPressMask | EnterWindowMask | LeaveWindowMask | SubstructureRedirectMask | SubstructureNotifyMask);
    // TODO XSetErrorHandler

    // Set cursor
    Cursor cursor = XCreateFontCursor(display, XC_left_ptr);
    XDefineCursor(display, root_window, cursor);
    XFreeCursor(display, cursor);

    for(running = 1; running; ) {
        XEvent e;
        XNextEvent(display, &e);
        handle_event(&e);
    }

    XCloseDisplay(display);
    exit(EXIT_SUCCESS);
}
